#ifndef MY_ITERATOR_HPP
#define MY_ITERATOR_HPP

#include <cassert>
#include <iostream> // for debugging
#include <iterator> // for iterator category tags

/*
Forward & bidirectional iterators requirements:

Iterator:
- CopyConstructible
- CopyAssignable
- Destructible
- Supports: *a (Dereferenceable)
- Supports: ++a (Preincrementable)

Input Iterator:
- All requirements of an iterator.
- Supports: == (EqualityComparable)
- Supports: !=
- Supports: ->
- Supports: a++ (Postincrementable)

Forward Iterator:
- All requirements of an input iterator
- DefaultConstructible
- Supports expression: *a++

Bidirectional Iterator:
- All requirements of a forward iterator
- Predecrementable
- Postdecrementable
- Supports expression: *a--

*/

// my iterator
template <typename T>
class MyIterator {
  public:
    // member types
    using value_type = T; // type of values obtained when dereferencing the
                          // iterator
    using difference_type = std::size_t; // signed integer type to represent
                                         // distance between iterators
    using reference = T&; // type of reference to type of values
    using pointer = T*; // type of pointer to the type of values
    using iterator_category = std::forward_iterator_tag; // category of
                                                         // the iterator
    // using iterator_category = std::bidirectional_iterator_tag; // category of
    //                                                            // the iterator

    // TODO: constructor
    MyIterator();

    // copy ctor
    MyIterator(MyIterator const&) = default;
    // Constructor with pointer
    MyIterator(pointer, pointer, pointer);
    // copy assignment
    MyIterator& operator=(MyIterator const&) = default;
    // dtor
    ~MyIterator() { }

    // TODO: operators
    reference operator*();

    MyIterator& operator++();

    MyIterator operator++(int);

    bool operator==(MyIterator other);

    bool operator!=(MyIterator other);

    reference operator->();

  private:

    // TODO: private members
    pointer ptr;
    pointer begin;
    pointer end;
    // TODO: private method "check" to prevent illegal operations
    void check();
};

// Default constructor:
template <typename T>
MyIterator<T>::MyIterator() : ptr(nullptr), begin(ptr), end(ptr) {}

// Constructor with pointer
template<typename T>
MyIterator<T>::MyIterator(MyIterator<T>::pointer p, MyIterator<T>::pointer b, MyIterator<T>::pointer e) : ptr(p), begin(b), end(e) {}

// Dereference
template<typename T>
typename MyIterator<T>::reference MyIterator<T>::operator*() {
  check();
  return *ptr;
}

// Pre increment
template <typename T>
MyIterator<T>& MyIterator<T>::operator++() {
  ++ptr;
  return *this;
}

// Post increment
template<typename T>
MyIterator<T> MyIterator<T>::operator++(int) {
  MyIterator tmp = *this;
  ++(*this);
  return tmp;
}

// Equality
template <typename T>
bool MyIterator<T>::operator==(MyIterator other) {
  return ptr == other.ptr;
}

// Inequality
template <typename T>
bool MyIterator<T>::operator!=(MyIterator other) {
  return ptr != other.ptr;
}

// Arrow
template<typename T>
typename MyIterator<T>::reference MyIterator<T>::operator->() {
  check();
  return **this;
}

// Check
template<typename T>
void MyIterator<T>::check() {
  assert(ptr < end && ptr >= begin);
}

#endif /* MY_ITERATOR_HPP */
