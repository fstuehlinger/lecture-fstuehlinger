#include <vector>
#include <bitset>

class Genome {
public:
    // Default constructor:
    Genome();
    // Copy constructor:
    Genome(const Genome& other);
    // Get number of bad genes:
    unsigned int badGens() const;
    // Mutate a genome:
    void mutate();
    // Set the mutation rate M:
    void setMutRate(unsigned int M_new);

private:
    static unsigned int N_; // Number of genes (size of gen[])
    static unsigned int M_; // Mutation rate
    std::bitset<100> gen_; // Genome
};

class Animal {
public:
    // Default constructor:
    Animal();
    // Copy constructor:
    Animal(const Animal& other);
    // Constructing from genome (birth):
    Animal(const Genome& g);
    // Get current Age:
    unsigned int getAge() const;
    // Check wether animal is alive:
    bool isAlive() const;
    // Check for pregnancy:
    bool isPregnant() const;
    // Returns the maximal age this animal can reach:
    unsigned int maxAge() const;
    // Increase age and give birth if pregnant:
    void incrAge();
    // Give birth if pregnan (return false if not)
    bool giveBirth();
    // Set threshold:
    void setT(unsigned int T_new);
    // Set reproduction age:
    void setRepAge(unsigned int age_new);
    // Set birth rate:
    void setBirthRate(unsigned int birthRate_new);

private:
    static unsigned int T_; // Threshold
    static unsigned int repAge_; // Reproduction age
    static unsigned int birthRate_; // Birth rate
    unsigned int age_; // Age
    Genome gen_; // Genome
    bool preg_; // Pregnancy
};

class Population {
public:
    // Default constructor:
    Population();
    // Construct with given population size:
    Population(unsigned int N);
    // Copy constructor:
    Population(const Population& other);
    // Deconstructor:
    ~Population();
    // Get size of the population:
    unsigned int getSize() const;
    // Get probability of an animal dying:
    double deathProb() const;
    // Iterate through all animals and apply a function f (no parameter):
    void iterateAnimals(void (*funcPtr)());

private:
    std::vector<Animal> pop_;
    static unsigned int N_max_;
};