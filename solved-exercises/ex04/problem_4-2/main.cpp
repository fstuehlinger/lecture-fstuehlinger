#include <iostream>
#include <cmath>
#include <numbers>
#include <cassert>
#include "simpson.hpp"

using data_t = double;

int main () {

    std::cout << "Bins: ";
    int N;
    std::cin >> N;

    data_t lamb;
    std::cout << "Lambda: ";
    std::cin >> lamb;

    auto func = [lamb] (data_t x) {return std::exp(-lamb*x);};
    std::cout << simpson(func, 0.0, 1.0, N) << std::endl;

    return 0;
}