/* Programming Techniques for Scientific Simulations, HS 2022
 * Exercise 4.1
 */

#include <complex>
#include <iostream>

enum Z2 { Plus, Minus };

template<class T>
T identity_element() { return T(1); }

// Specialisation for Z2
template<>
Z2 identity_element() {
  return Plus;
}

// PRE: -
// POSt: Result of multiplication in Z2 with a and b.
Z2 operator*(const Z2 a, const Z2 b)
{
  if (a == b) {
    return Plus;
  } else {
    return Minus;
  }
}

// PRE: -
// POST: Outputs "Plus" or "Minus" respectively.
std::ostream& operator<<(std::ostream& os, const Z2 a)
{
  if (a == Plus) {
    os << "Plus";
  } else {
    os << "Minus";
  }
  return os;
}

// PRE: T has to be a number type (int, double, float, complex, ...)
//      that has a unary - operator.
// POST: Multiplication of a with 1/-1.
template<class T>
T operator*(const T a, const Z2 b)
{
  if (b == Plus) {
    return a;
  } else {
    return -a;
  }
}

// PRE: T has to be a number type (int, double, float, complex, ...)
//      that has a unary - operator.
// POST: Multiplication of a with 1/-1.
template<class T>
T operator*(const Z2 b, const T a)
{
  if (b == Plus) {
    return a;
  } else {
    return -a;
  }
}

// PRE: T has to have a multipligation (*) operator and an identity element.
// POST: The n-th power of a.
template<class T>
T mypow(T a, unsigned int n)
{
  if (n == 0) {
    return identity_element<T>();
  } else {
    T res = a;
    for (unsigned i = 2; i <= n; ++i) {
      res = res * a;
    }
    return res;
  }
}

int main()
{
  // Some testing: feel free to add your own!
  std::cout << "Plus*Minus = " << Plus*Minus << std::endl;
  std::cout << "Plus*-1*Minus = " << Plus*-1*Minus << std::endl;
  std::cout << "(1.+3.)*mypow(Minus,4) = " << (1.+3.)*mypow(Minus,4)
            << std::endl;

  std::cout << "Id = " << identity_element<Z2>() << std::endl;

  for (unsigned i=0; i<7; ++i)
    std::cout << "Plus^" << i << " = " << mypow(Plus,i) << std::endl;
  for (unsigned i=0; i<7; ++i)
    std::cout << "Minus^" << i << " = " << mypow(Minus,i) << std::endl;
  for (unsigned i=0; i<7; ++i)
    std::cout << "2^" << i << " = " << mypow(2.0,i) << std::endl;

  // For complex numbers
  std::cout << "Plus*-std::complex<double>(1., 2.)*Minus = "
            <<  Plus*-std::complex<double>(1., 2.)*Minus
            << std::endl;
  std::cout << "std::complex<double>(1., 1.)*mypow(Minus,4) = "
            <<  std::complex<double>(1., 1.)*mypow(Minus,4)
            << std::endl;
  for (unsigned i=0; i<7; ++i)
    std::cout << "std::complex<double>(1., 1.)^" << i << " = "
              << mypow(std::complex<double>(1., 1.),i)
              << std::endl;

  return 0;
}
