add_library(time_lib STATIC timer.cpp)

target_include_directories(time_lib PUBLIC ./)

install(TARGETS time_lib
        ARCHIVE DESTINATION time_lib
        LIBRARY DESTINATION time_lib
        RUNTIME DESTINATION bin
        )

install(FILES timer.hpp DESTINATION include)