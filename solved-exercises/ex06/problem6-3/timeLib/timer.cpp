#include "timer.hpp"
#include <chrono>

Timer::Timer() : startPoint(std::chrono::high_resolution_clock::time_point::min()), endPoint(startPoint), running(false) {}

void Timer::start() {
    if (running) {
        throw "Timer already runnig";
    } else {
        running = true;
        startPoint = std::chrono::high_resolution_clock::now();
    }
}

void Timer::stop() {
    if (!running) {
        throw "Timer not running";
    } else {
        running =false;
        endPoint = std::chrono::high_resolution_clock::now();
    }
}

double Timer::duration() {
    if (running) {
        throw "Timer still running";
    } else {
        diff_sec = endPoint - startPoint;
        return diff_sec.count();
    }
}