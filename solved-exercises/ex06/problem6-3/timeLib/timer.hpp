#ifndef GENOME_HPP
#define GENOME_HPP

#include <chrono>

class Timer {
public:

    // Default constructor (starPoint = endPoint = min, running = false):
    Timer();

    // Sart the timer:
    // PRE: This timer is currently NOT running.
    // POST: Time is measured until stop is called.
    // Exceptions: Throws "Timer already running", if PRE is violated.
    void start();

    // Stop the timer:
    // PRE: This timer IS currently running.
    // POST: The measurement ends and the duration is saved.
    // Exceptions: Throws "Timer not running", if PRE is violated.
    void stop();

    // Get time that passed between start and stop calls.
    // PRE: Timer must not be running.
    // POST: The duration in seconds.
    // Exceptions: Throws "Timer still running", if PRE is violated.
    double duration();

private:

    // Start time of the Timer:
    std::chrono::time_point<std::chrono::high_resolution_clock> startPoint;

    // End time of the Timer:
    std::chrono::time_point<std::chrono::high_resolution_clock> endPoint;

    // Difference of end and start in seconds:
    std::chrono::duration<double> diff_sec;

    // Bool that says wether the timer is currently running:
    bool running;
};

#endif