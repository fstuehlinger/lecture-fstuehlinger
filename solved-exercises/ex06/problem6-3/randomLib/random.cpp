#include "random.hpp"
#include <cmath>
#include <limits>

unsigned int Generator::a_ = 1664525;
unsigned int Generator::c_ = 1013904223;

Generator::Generator() : X_(0) {}

Generator::Generator(unsigned seed) : X_(seed) {};

unsigned int Generator::generate() {
    X_ = (a_*X_ + c_);
    return X_;
}

unsigned int Generator::max() {
    return -1;
}