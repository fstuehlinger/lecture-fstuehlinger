
class Generator {
public:
    // Default constructor:
    // PRE: -
    // POST: Generator with seed 0.
    // Exceptions: -
    Generator();

    // Constructor with seed:
    // PRE: -
    // POST: Generator with seed 'seed'.
    // Exceptions: -
    Generator(unsigned seed);

    // Generate random number:
    // PRE: -
    // POST: A psedo random number.
    // Exceptions: -
    unsigned int generate();

    // Return the biggest number that can be generated:
    // PRE: -
    // POST: Max number of unsigned int.
    // Exceptions: -
    unsigned int max();

private:
    // The last random number or the seed:
    unsigned int X_;

    // Parameter a (0 < a < m):
    static unsigned int a_;

    // Parameter c (0 <= c < m):
    static unsigned int c_;
};