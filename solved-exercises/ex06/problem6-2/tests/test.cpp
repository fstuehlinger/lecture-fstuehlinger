#include "genome.hpp"
#include <iostream>

int main() {

    unsigned errCount = 0;
    // Test default constructor:
    try {
        Genome g;
    } catch (...) {
        std::cout << "Wrong default initialization of genome (has bad genes)." << std::endl;
        ++errCount;
    }

    // Test copy constructor:
    try {
        Genome g;
        Genome::setMutRate(12);
        g.mutate();
        Genome h (g);
    } catch (...) {
        std::cout <<  "Wrong copy construction (not perfect)." << std::endl;
        ++errCount;
    }

    if (errCount == 0) {
        std::cout << "All tests passed." << std::endl;
    } else {
        std::cout << errCount << " errors." << std::endl;
    }

    return 0;
}