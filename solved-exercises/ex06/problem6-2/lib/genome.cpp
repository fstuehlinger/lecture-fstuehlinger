#include "genome.hpp"
#include <bitset>
#include <iostream>
#include <random>
#include <vector>

unsigned int Genome::M_;
unsigned int Genome::N_;

Genome::Genome() {
    if (badGens() != 0) {
        throw "Wrong initialization of gene (as bad genes by default).";
    }
}

Genome::Genome(const Genome& g) {
    for (unsigned i = 0; i < 100; ++i) {
        gen_[i] = g.gen_[i];
    }
    for (unsigned i = 0; i < 100; ++i) {
        if (gen_[i] != g.gen_[i]) {
            throw "Copy constructor isn't perfect.";
        }
    }
}

unsigned int Genome::badGens() const {
    return gen_.count();
}

unsigned int Genome::maxAge(unsigned int thresh) const {
    unsigned int age = 0;
    unsigned int muts = 0;
    for (;age < 100 && muts <= thresh; ++age) {
        if (gen_.test(age)) {
            ++muts;
        }
    }
    return age;
}

void Genome::mutate() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0,99);

    std::vector<unsigned> muts (M_, -1);
    for (unsigned i = 0; i < M_;) {
        unsigned rand = distrib(gen);
        bool New = true;
        for (unsigned e : muts) {
            if (e == rand) {
                New = false;
                break;
            }
        }
        if (New) {
            gen_.flip(rand);
            ++i;
        }
    }
}

void Genome::setMutRate(unsigned int M_new) {
    M_ = M_new;
}
