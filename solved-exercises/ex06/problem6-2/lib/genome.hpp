#ifndef GENOME_HPP
#define GENOME_HPP

#include <bitset>

class Genome {
public:
    // Default constructor:
    Genome();
    // Copy constructor:
    Genome(const Genome& other);
    // Get number of bad genes:
    unsigned int badGens() const;
    // Helper function for 'Animal::maxAge()':
    unsigned int maxAge(unsigned int thresh) const;
    // Mutate a genome:
    void mutate();
    // Set the mutation rate M:
    static void setMutRate(unsigned int M_new);

private:
    static unsigned int N_; // Number of genes (size of gen[])
    static unsigned int M_; // Mutation rate
    std::bitset<100> gen_; // Genome
};

#endif