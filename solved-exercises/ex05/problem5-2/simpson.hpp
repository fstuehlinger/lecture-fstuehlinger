#include <cassert>
// Computes the integral of the function of fcnPtr over the intervall (intStart, intEnd)
// with simpson integration, using "bins" amount of bins.

// Template requirements: 
// F has to be a function object with an ()-operator. It's output should be of type "data_t".
// It should take one argument of typ "data_t" aswell.
// PRE: intStart <= intEnd; bins > 0.
// POST: The value of the integral with the respective precision for the amount of bins.
template<typename F, typename data_t>
data_t simpson(F func, data_t intStart, data_t intEnd, int bins) {
    // intEnd has to be bigger than intStart:
    assert(intEnd >= intStart);
    // bins has to be positive:
    assert(bins > 0);
    
    // Discretize intervall:
    data_t deltaX = (intEnd - intStart) / bins;

    data_t result = 0;
    // Keep track of loop count for picking the correct factors (1, 2 or 4):
    unsigned count = 0;
    
    // Computation:
    for (data_t x = intStart; x <= intEnd; x += deltaX/2.0) {
        if (count == 0) {
            result += func(x);
        } else  if (x != intEnd) {
            if (count % 2 == 0) {
                result += 2.0 * func(x);
            } else {
                result += 4.0 * func(x);
            }
        } else {
            result += func(intEnd);
        }
        ++count;
    }

    result *= (deltaX/6.0);

    return result;
}