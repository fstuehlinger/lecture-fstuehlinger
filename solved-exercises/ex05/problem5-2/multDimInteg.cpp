#include "simpson.hpp"
#include <iostream>
#include <cmath>

// using data_t = double;
template <typename F, typename data_t, typename B, typename C>
data_t multiSimpson(F func, data_t a, data_t b, B lb, C ub, int binsX, int binsY) {
    data_t deltaX = (b-a) / (double)binsX;
    data_t result = 0;
    unsigned count = 0;
    for (data_t x = a; x <= b; x += deltaX/2.0) {
        data_t c = lb(x);
        data_t d = ub(x);
        auto lamb = [x, &func] (data_t y) {return func(x,y);};
        if (count == 0) {
            result += simpson(lamb, c, d, binsY);
        } else  if (x != b) {
            if (count % 2 == 0) {
                result += 2.0 * simpson(lamb, c, d, binsY);
            } else {
                result += 4.0 * simpson(lamb, c, d, binsY);
            }
        } else {
            result += simpson(lamb, c, d, binsY);
        }
        ++count;
    }
    result *= (deltaX/6.0);
    return result;
}

int main () {

    auto f = [] (double x, double y) {return 1.0;};
    auto lb = [] (double x) {return -std::sqrt(1.0001-std::pow(x,2));};
    auto ub = [] (double x) {return std::sqrt(1.0001-std::pow(x,2));};

    double result = multiSimpson(f, -1.0, 1.0, lb, ub, 100, 100);

    std::cout << "Unit disk: " << result << std::endl;

    double R = 1.7606;

    auto f2 = [] (double x, double y) {return std::exp(-(std::pow(x,2)+std::pow(y,2)));};
    auto lb2 = [R] (double x) {return -std::sqrt(std::pow(R,2)+0.0001-std::pow(x,2));};
    auto ub2 = [R] (double x) {return std::sqrt(std::pow(R,2)+0.0001-std::pow(x,2));};

    double other = multiSimpson(f2, -R, R, lb2, ub2, 100, 100);

    std::cout << "Exp: " << other << std::endl;

    return 0;
}