#include <cassert>

// Traits for function objects with input/output type member
template <typename F> struct domain_t {
    typedef typename F::input_t type;
};

template <typename F> struct result_t {
    typedef typename F::output_t type;
};

// Computes the integral of the function of fcnPtr over the intervall (intStart, intEnd)
// with simpson integration, using "bins" amount of bins.

// Template requirements: 
// F has to be a function object with an ()-operator. It's output should be of type "data_t".
// It should take one argument of typ "data_t" aswell.
// PRE: intStart <= intEnd; bins > 0.
// POST: The value of the integral with the respective precision for the amount of bins.
template<typename F>
typename result_t<F>::type simpson(F func, typename domain_t<F>::type intStart, typename domain_t<F>::type intEnd, int bins) {
    using data_t = typename domain_t<F>::type;
    
    // intEnd has to be bigger than intStart: Didn't find a convenient way to make this test work for complex numbers.
    //assert(intEnd >= intStart);
    // bins has to be positive:
    assert(bins > 0);
    
    // Discretize intervall:
    data_t deltaX = (intEnd - intStart) / (data_t)bins;

    typename result_t<F>::type result = 0;
    
    // Keep track of loop count for picking the correct factors (1, 2 or 4):
    unsigned count = 0;
    // Computation:
    data_t x = intStart;
    for (int i = 0; i < 2*bins + 1; ++i) {
        if (count == 0) {
            result += func(x);
        } else  if (x != intEnd) {
            if (count % 2 == 0) {
                result += 2.0 * func(x);
            } else {
                result += 4.0 * func(x);
            }
        } else {
            result += func(intEnd);
        }
        ++count;
        x += deltaX/2.0;
    }

    result *= (deltaX/6.0);

    return result;
}

