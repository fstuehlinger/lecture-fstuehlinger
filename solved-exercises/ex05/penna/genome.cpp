#include "genome.hpp"
#include <bitset>
#include <iostream>
#include <experimental/random>

unsigned int Genome::M_;
unsigned int Genome::N_;

Genome::Genome() {
}

Genome::Genome(const Genome& g) {
    for (unsigned i = 0; i < 100; ++i) {
        gen_[i] = g.gen_[i];
    }
}

unsigned int Genome::badGens() const {
    return gen_.count();
}

unsigned int Genome::maxAge(unsigned int thresh) const {
    unsigned int age = 0;
    unsigned int muts = 0;
    for (;age < 100 && muts <= thresh; ++age) {
        if (gen_.test(age)) {
            ++muts;
        }
    }
    return age;
}

void Genome::mutate() {
    for (unsigned i = 0; i < M_; ++i) {
        unsigned rand = std::experimental::randint(0,99);
        gen_.flip(rand);
    }
}

void Genome::setMutRate(unsigned int M_new) {
    M_ = M_new;
}
