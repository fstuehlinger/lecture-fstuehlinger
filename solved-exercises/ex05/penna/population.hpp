#ifndef POPULATION_HPP
#define POPULATION_HPP

#include <vector>
#include "animal.hpp"
#include "genome.hpp"

class Population {
public:
    // Default constructor:
    Population();
    // Construct with given population size:
    Population(unsigned int N);
    // Copy constructor:
    Population(const Population& other);
    // Deconstructor:
    ~Population();
    // Get size of the population:
    unsigned int getSize() const;
    // Get probability of an animal dying:
    double deathProb() const;
    // Iterate through all animals and apply a function f (no parameter):
    void iterateAnimals(void (*funcPtr)(Animal));

private:
    std::vector<Animal> pop_;
    static unsigned int N_max_;
};

#endif