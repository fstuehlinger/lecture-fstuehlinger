#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include "genome.hpp"

class Animal {
public:
    // Default constructor:
    Animal();
    // Copy constructor:
    Animal(const Animal& other);
    // Constructing from genome (birth):
    Animal(const Genome& g);
    // Get current Age:
    unsigned int getAge() const;
    // Check wether animal is alive:
    bool isAlive() const;
    // Check for pregnancy:
    bool isPregnant() const;
    // Returns the maximal age this animal can reach:
    unsigned int maxAge() const;
    // Increase age and give birth if pregnant:
    void incrAge();
    // Give birth if pregnan (return pointer the child)
    Animal *giveBirth();
    // Set threshold:
    void setT(unsigned int T_new);
    // Set reproduction age:
    void setRepAge(unsigned int age_new);
    // Set birth rate:
    void setBirthRate(unsigned int birthRate_new);

private:
    static unsigned int T_; // Threshold
    static unsigned int repAge_; // Reproduction age
    static unsigned int birthRate_; // Birth rate
    unsigned int age_; // Age
    Genome genome_; // Genome
    bool preg_; // Pregnancy
};

#endif