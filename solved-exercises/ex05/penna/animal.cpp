#include "animal.hpp"
#include <bitset>
#include <iostream>

unsigned int Animal::T_;
unsigned int Animal::repAge_;
unsigned int Animal::birthRate_;

Animal::Animal() : age_(0), preg_(false) {}

Animal::Animal(const Animal& other) : genome_(other.genome_), age_(other.age_), preg_(other.preg_) {}

Animal::Animal(const Genome& g) : genome_(g), age_(0), preg_(false) {}

unsigned int Animal::getAge() const {
    return age_;
}

bool Animal::isAlive() const {
    if (genome_.badGens() > T_) {
        return false;
    }
    return true;
}

bool Animal::isPregnant() const {
    return preg_;
}

unsigned int Animal::maxAge() const {
    return genome_.maxAge(T_);
}

void Animal::incrAge() {
    ++age_;
    if (age_ >= repAge_) {
        giveBirth();
    }
}

Animal *Animal::giveBirth() {
    if (preg_) {
        Genome newGen = genome_;
        newGen.mutate();
        Animal *child = new Animal(newGen);
        preg_ == false;
        return child;
    }
    return NULL;
}

void Animal::setT(unsigned int newT) {
    T_ = newT;
}

void Animal::setRepAge(unsigned int newR) {
    repAge_ = newR;
}

void Animal::setBirthRate(unsigned int newB) {
    birthRate_ = newB;
}