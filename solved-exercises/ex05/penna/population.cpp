#include "population.hpp"
#include <iostream>

unsigned int Population::N_max_;

Population::Population() {};

Population::Population(unsigned int N) : pop_(std::vector<Animal> (N)) {}

Population::Population(const Population& other) : pop_(other.pop_) {}

Population::~Population() {
    pop_.~vector();
}

unsigned int Population::getSize() const {
    return pop_.size();
}

double Population::deathProb() const {
    return 1 - (((double)pop_.size()) / (double)N_max_);
}

void Population::iterateAnimals(void (*funcPtr)(Animal)) {
    for (Animal a : pop_) {
        (*funcPtr)(a);
    }
}