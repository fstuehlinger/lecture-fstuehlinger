#include <iostream>
#include <cmath>
#include <numbers>

int main () {

    int N;
    std::cin >> N;

    double deltaX = 1.0 / N;

    double arg = 0;
    double result = 0;
    for (int i = 0; arg <= 1; ++i) {
        if (i == 0) {
            result += arg * (1 - arg);
        } else  if (arg != M_PI) {
            if (i % 2 == 0) {
                result += 2 * arg * (1 - arg);
            } else {
                result += 4 * arg * (1 - arg);
            }
        } else {
            result += arg * (1-arg);
        }
        arg += deltaX / 2.0;
    }

    result *= (deltaX/6);

    std::cout << "Result = " << result << std::endl;

    return 0;
}