#include <iostream>
#include <cmath>
#include <numbers>

int main () {

    int N;
    std::cin >> N;

    double deltaX = M_PI / N;

    double arg = 0;
    double result = 0;
    for (int i = 0; arg <= M_PI; ++i) {
        if (i == 0) {
            result += std::sin(arg);
        } else  if (arg != M_PI) {
            if (i % 2 == 0) {
                result += 2.0 * std::sin(arg);
            } else {
                result += 4.0 * std::sin(arg);
            }
        } else {
            result += std::sin(M_PI);
        }
        arg += deltaX / 2.0;
    }

    result *= (deltaX/6.0);

    std::cout << "Result = " << result << std::endl;

    return 0;
}