#include <iostream>
#include <cmath>

int main() {

    double eps = 0;

    while (1.0 + eps == 1.0) {
        eps += 10e-25;
    }

    std::cout << "Epsilon(Approx) = " << eps << std::endl;
    std::cout << "Actual = " << std::numeric_limits<double>::epsilon();

    return 0;
}