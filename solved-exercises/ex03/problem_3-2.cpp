#include <iostream>
#include <cmath>
#include <limits>

template<typename T>
void epsilon(T dummy) {

    T eps = 1;
    T one = 1;
    T half = 0.5;

    while (one + eps * half != 1.0) {
        eps *= half;
    }

    std::cout << "Epsilon(Approx) = " << eps << std::endl;
    std::cout << "Actual = " << std::numeric_limits<T>::epsilon() << std::endl;

}

int main() {

    char t;
    std::cout << "Type? (d, f or l): ";
    std::cin >> t;

    switch(t) {
        case 'd':
            double d;
            epsilon(d);
            break;
        case 'f':
            float f;
            epsilon(f);
            break;
        case 'l':
            // long l;

            // kszenes: The task was to determine epsilon for the `long double`
            // type not `long`. The `long` type refers to an integer which is
            // at least as precise as `int`. 
            // The `long double` however is a floating-point type which is at
            // least as precise as `double`. (Machine epsilon really only makes
            // sense for floating-point arithmetic)
            // Note that I say "as least as precise" because the number of 
            // actual bytes allocated per type is hardware specific. You can get
            // the number of bytes allocated for that type on your machine
            // using the `sizeof` function in C/C++ (e.g. `sizeof(long double)`)
            long double l;
            epsilon(l);
    }
    
    return 0;
}
