#include <iostream>
#include <vector>

using data_t = double;

// COMMENT: function declaration only needed if function definition
// succedes the region where it is called from
// void printDynamic(std::vector<data_t> a);

void fillDynamic(std::vector<data_t>& a, int n) {
    for (unsigned i = 0; i < n; ++i) {
        std::cin >> a[i];
    }
}

// COMMENT: arrays are passed to functions through their pointers, hence
// no data is being copied and you do not need to pass them by reference
void fillStatic(data_t a[], int n) {
    for (unsigned i = 0; i < n; ++i) {
        std::cin >> a[i];
    }
}

void normalizeDynamic(std::vector<data_t>& a) {
    data_t sum = 0;
    for (data_t e : a) {
        sum += e;
    }
    for (data_t& e : a) {
        e = e / sum;
    }
}

void normalizeStatic(data_t a[], int n) {
    data_t sum = 0;
    for (unsigned i = 0; i < n; ++i) {
        sum += a[i];
    }
    for (unsigned i = 0; i < n; ++i) {
        a[i] = a[i] / sum;
    }
}

void printDynamic(std::vector<data_t>& a) {
    for (unsigned i = 1; i <= a.size(); ++i) {
        std::cout << a[a.size() - i] << " ";
    }
    std::cout << std::endl;
}

void printStatic(data_t a[], int n) {
    for (unsigned i = 1; i <= n; ++i) {
        std::cout << a[n - i] << " ";
    }
    std::cout << std::endl;
}

int main () {

    int size;
    std::cin >> size;

    // If how == 1 -> dynamic, if how == 2 -> static.
    // COMMENT: you could use a bool data type for `how` which clarifies its
    // purpose
    int how;
    std::cin >> how;


    if (how == 1) {
        std::vector<data_t> array (size);
        fillDynamic(array, size);
        normalizeDynamic(array);
        printDynamic(array);
    } else if (how == 2) {
        data_t array[size];
        fillStatic(array, size);
        normalizeStatic(array, size);
        printStatic(array, size);
    }
    // COMMENT: else block can be omitted since you call `return 0` anyways 
    // else {
    //     return 0;
    // }

    return 0;
}