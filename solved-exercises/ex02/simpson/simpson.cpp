#include <cassert>
#include "lib/simpson.hpp"

using data_t = double;


data_t simpson(data_t (*fcnPtr)(data_t), data_t intStart, data_t intEnd, int bins) {
    // intEnd has to be bigger than intStart:
    assert(intEnd >= intStart);
    // bins has to be positive:
    assert(bins > 0);
    
    // Discretize intervall:
    data_t deltaX = (intEnd - intStart) / bins;

    data_t result = 0;
    // Keep track of loop count for picking the correct factors (1, 2 or 4):
    unsigned count = 0;
    
    // Computation:
    for (data_t x = intStart; x <= intEnd; x += deltaX/2.0) {
        if (count == 0) {
            result += (*fcnPtr)(x);
        } else  if (x != intEnd) {
            if (count % 2 == 0) {
                result += 2.0 * (*fcnPtr)(x);
            } else {
                result += 4.0 * (fcnPtr)(x);
            }
        } else {
            result += (*fcnPtr)(intEnd);
        }
        ++count;
    }

    result *= (deltaX/6.0);

    return result;
}