# make simpson

# COMMENT: type alias is a C++11 extension, you should provide
# the compiler with this flag
CXXFLAGS = -std=c++11

.PHONY: all
all: result.txt clean

simpson.o: simpson.cpp
	c++ $(CXXFLAGS) -c -Ilib simpson.cpp 

# COMMENT: make sure to include compilation targets for all files.
# Note that libraries are compiled binaries and are hence hardware specific
# You should only upload source files and provide the user with makefile targets
# to compile by themselves
libsimpson.a: simpson.o
	ar -crs libsimpson.a simpson.o

simpson-sinus.o: simpson-sinus.cpp
	c++ $(CXXFLAGS) -c -Ilib simpson-sinus.cpp

simpson-sinus: simpson-sinus.o libsimpson.a
	c++ $(CXXFLAGS) -o simpson-sinus simpson-sinus.o -L. -lsimpson

result.txt: simpson-sinus
	./simpson-sinus > result.txt

clean:
	rm *.o

.PHONY: cleanAll
cleanAll:
	rm -v *.o simpson-sinus libsimpson.a result.txt