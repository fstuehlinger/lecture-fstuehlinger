#include <iostream>
#include <cmath>
#include <numbers>
#include <cassert>
#include "lib/simpson.hpp"

int main () {

    for (unsigned N = 3; N <= 12; ++N) {
        std::cout << N << "    " << simpson(&std::sin, 0, M_PI, N) << std::endl;
    }

    return 0;
}