    
using data_t = double;

// Computes the integral of the function of fcnPtr over the intervall (intStart, intEnd)
// with simpson integration, using "bins" amount of bins.

// PRE: intStart <= intEnd; bins > 0.
// POST: The value of the integral with the respective precision for the amount of bins.
data_t simpson(data_t (*fcnPtr)(data_t), data_t intStart, data_t intEnd, int bins);